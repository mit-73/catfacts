package com.mit.catfacts.utils

enum class WorkMode { Online, Offline }