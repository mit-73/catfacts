package com.mit.catfacts.utils

import androidx.fragment.app.Fragment
import com.mit.catfacts.presentation.detailsfragment.DetailsFragment
import com.mit.catfacts.presentation.factsfragment.FactsFragment
import com.mit.catfacts.presentation.favoritefragment.FavoriteFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

class Screens {
    class Facts(var animal: String) : SupportAppScreen() {
        override fun getFragment(): Fragment {
            return FactsFragment().animalInstance(animal)
        }
    }

    class Details(var id: String) : SupportAppScreen() {
        override fun getFragment(): Fragment {
            return DetailsFragment().idInstance(id)
        }
    }

    class Favorite : SupportAppScreen() {
        override fun getFragment(): Fragment {
            return FavoriteFragment()
        }
    }
}