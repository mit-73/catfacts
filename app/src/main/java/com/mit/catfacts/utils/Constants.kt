package com.mit.catfacts.utils

const val BASE_URL = "https://cat-fact.herokuapp.com"
const val REQUEST_TIMEOUT: Long = 10

const val DB_NAME = "catfacts.db"
const val TAB_ANIMAL = "animalstab"
const val TAB_FAVORITE = "favoritestab"

const val ANIMAL_CATS = "cat"
const val ANIMAL_DOGS = "dog"
const val ANIMAL_SNAILS = "snail"
const val ANIMAL_HORSES = "horse"
const val ANIMAL_CHOSEN = "chosen"