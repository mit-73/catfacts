package com.mit.catfacts.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.mit.catfacts.data.db.entity.FavoriteEntity
import com.mit.catfacts.utils.TAB_FAVORITE

@Dao
interface FavoriteDAO{
    @Insert
    fun addFavorite(unit: FavoriteEntity)

    @Query("SELECT *  FROM $TAB_FAVORITE")
    suspend fun getFavorites() : List<FavoriteEntity>

    @Query("DELETE FROM $TAB_FAVORITE where id = :id")
    suspend fun removeFavorite(id : String)


}