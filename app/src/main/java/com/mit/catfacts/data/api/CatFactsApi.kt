package com.mit.catfacts.data.api

import com.mit.catfacts.domain.dataclass.ApiModel
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface CatFactsApi {
    @GET("/facts/random?")
    fun getRandomFactsAsync(@Query("animal_type") animal: String, @Query("amount") amount: Int): Deferred<Response<List<ApiModel>>>

    @GET("/facts/{id}")
    fun getFactByIdAsync(@Path("id") id: String): Deferred<Response<ApiModel>>
}