package com.mit.catfacts.data.db.entity

import androidx.room.Entity
import com.google.gson.annotations.SerializedName
import com.mit.catfacts.utils.TAB_FAVORITE

@Entity(primaryKeys = ["id"], tableName = TAB_FAVORITE)
data class FavoriteEntity (
    @field:SerializedName("id")
    var id : String = ""
)