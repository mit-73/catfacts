package com.mit.catfacts.data.db

import androidx.room.Database
import androidx.room.DatabaseConfiguration
import androidx.room.InvalidationTracker
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteOpenHelper
import com.mit.catfacts.data.db.dao.FactsDAO
import com.mit.catfacts.data.db.dao.FavoriteDAO
import com.mit.catfacts.data.db.entity.FactsEntity
import com.mit.catfacts.data.db.entity.FavoriteEntity

@Database(entities = [FactsEntity::class, FavoriteEntity::class], version = 1, exportSchema = false)
abstract class CatFactsDatabase : RoomDatabase(){
    abstract fun factsDAO() : FactsDAO
    abstract fun favoriteDao() : FavoriteDAO
    override fun createOpenHelper(config: DatabaseConfiguration?): SupportSQLiteOpenHelper {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun createInvalidationTracker(): InvalidationTracker {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun clearAllTables() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}