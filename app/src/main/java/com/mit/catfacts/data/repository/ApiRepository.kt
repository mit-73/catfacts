package com.mit.catfacts.data.repository

import com.mit.catfacts.data.db.entity.FactsEntity
import com.mit.catfacts.data.db.entity.FavoriteEntity
import com.mit.catfacts.domain.dataclass.ApiModel
import com.mit.catfacts.domain.dataclass.FactModel
import com.mit.catfacts.domain.interactor.ApiInteractor
import com.mit.catfacts.domain.interactor.LoadFactsInteractor
import com.mit.catfacts.domain.interactor.LoadFavoriteInteractor
import com.mit.catfacts.utils.Settings
import com.mit.catfacts.utils.WorkMode

class ApiRepository constructor(
    private val api: ApiInteractor,
    private val dbFacts: LoadFactsInteractor,
    private val dbFavorite: LoadFavoriteInteractor
) {

    suspend fun getFacts(animal: String, amount: Int): MutableList<FactModel> {
        return when (Settings.currentWorkMode) {
            WorkMode.Online -> getAllFromNet(animal, amount)
            else -> getAllFromDatabase(animal)
        }
    }

    suspend fun getById(id: String): String? {
        return when (Settings.currentWorkMode) {
            WorkMode.Online -> getFactFromNet(id)
            else -> getFactFromDatabase(id)
        }
    }

    suspend fun getFavorite(): MutableList<FactModel> {
        val list = mutableListOf<FactModel>()
        val favoriteList = getFavoriteList()
        for (it in favoriteList) {
            var fact = ""
            dbFacts.executeSave(it) {
                fact = it
            }
            list.add(FactModel(it, fact, true))
        }
        return list
    }

    suspend fun updateFavorite(id: String, isFavorite: Boolean) {
        when (isFavorite) {
            true -> dbFavorite.executeAddFavorite(FavoriteEntity(id)) {}
            false -> dbFavorite.executeRemoveFavorite(id) {}
        }
    }

    private suspend fun getFavoriteList(): List<String> {
        val favoriteList = mutableListOf<String>()
        for (it in dbFavorite.executeGetFavorites())
            favoriteList.add(it.id)
        return favoriteList
    }

    private fun getFactFromNet(id: String): String? {
        var result = ""
        api.executeId(id) {
            result = it
        }
        return result
    }

    private suspend fun getAllFromNet(animal: String, amount: Int): MutableList<FactModel> {
        val list = mutableListOf<FactModel>()
        val listOfUnits = mutableListOf<FactsEntity>()
        var requestResponse: List<ApiModel>? = null
        api.executeRandom(animal, amount) {
            requestResponse = it
        }
        if (requestResponse == null) {
            Settings.currentWorkMode = WorkMode.Offline
            return getAllFromDatabase(animal)
        }
        val favoriteList = getFavoriteList()
        for (it in requestResponse!!) {
            val isFavorite = favoriteList.contains(it._id)
            list.add(FactModel(it._id, it.text, isFavorite))
            listOfUnits.add(FactsEntity(it._id, it.type, it.text))
        }
        dbFacts.executeInsertFacts(listOfUnits) {}
        return list
    }

    private fun getFactFromDatabase(id: String): String {
        var text = ""
        dbFacts.executeText(id) {
            text = it
        }
        return text
    }

    private suspend fun getAllFromDatabase(animal: String): MutableList<FactModel> {
        val list = mutableListOf<FactModel>()
        var request: List<FactsEntity>? = null
        dbFacts.executeSavedFacts(animal) {
            request = it
        }
        val favoriteList = getFavoriteList()
        for (it in request!!) {
            val isFavorite = favoriteList.contains(it.id)
            list.add(FactModel(it.id, it.text, isFavorite))
        }
        return list
    }
}