package com.mit.catfacts.data.db.entity

import androidx.room.Entity
import com.google.gson.annotations.SerializedName
import com.mit.catfacts.utils.TAB_ANIMAL

@Entity(primaryKeys = ["id"], tableName = TAB_ANIMAL)
data class FactsEntity (
    @field:SerializedName("id")
    var id : String = "",
    @field:SerializedName("animal")
    var animal : String = "",
    @field:SerializedName("text")
    var text : String = ""
)