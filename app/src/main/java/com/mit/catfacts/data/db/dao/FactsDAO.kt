package com.mit.catfacts.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.mit.catfacts.data.db.entity.FactsEntity
import com.mit.catfacts.utils.TAB_ANIMAL

@Dao
interface FactsDAO {
    @Query("SELECT  *FROM $TAB_ANIMAL where animal = :animal")
    suspend fun getSavedFacts(animal: String): List<FactsEntity>

    @Query("SELECT text FROM $TAB_ANIMAL where id = :id")
    suspend fun getText(id: String): String

    @Query("SELECT * FROM $TAB_ANIMAL where id = :id")
    suspend fun getRecord(id: String): FactsEntity

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertFacts(facts: List<FactsEntity>)
}