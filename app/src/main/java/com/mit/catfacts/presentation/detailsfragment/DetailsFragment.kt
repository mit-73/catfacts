package com.mit.catfacts.presentation.detailsfragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.mit.catfacts.R
import kotlinx.android.synthetic.main.details_fragment.*

class DetailsFragment : DetailsView, MvpAppCompatFragment() {
    @InjectPresenter
    lateinit var presenter: DetailsPresenter

    private var id = arguments!!.getString("id")

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.details_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter.loadDetail(id)
    }

    fun idInstance(id: String): DetailsFragment {
        val factDetailsFragment = DetailsFragment()

        val args = Bundle()
        args.putString("id", id)
        factDetailsFragment.arguments = args

        return factDetailsFragment
    }

    override fun onShowDetails(id: String, text: String) {
        tvId.text = id
        tvText.text = text
    }
}