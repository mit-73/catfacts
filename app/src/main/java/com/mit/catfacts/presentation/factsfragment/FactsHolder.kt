package com.mit.catfacts.presentation.factsfragment

import android.view.View
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mit.catfacts.App
import kotlinx.android.synthetic.main.item_fact.view.*

class FactsHolder(
    itemView: View,
    private val clickListener: (Int, String) -> Unit,
    private val clicker: (String, Boolean) -> Unit
) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
    private var isFavorite = false
    var factText = itemView.tvFact as TextView
    var animalType = itemView.tvAnimalType as TextView
    private var button: ImageButton = itemView.btnFavorite

    init {
        itemView.setOnClickListener(this)

        button.setOnClickListener {
            if (isFavorite) isFavorite = false
            else if (!isFavorite) isFavorite = true
            setFavoriteState(isFavorite)
            clicker.invoke(animalType.text.toString(), isFavorite)
        }
    }

    fun setFavoriteState(favoriteState: Boolean) {
        isFavorite = favoriteState
        when (isFavorite) {
            true -> button.setImageDrawable(
                App.application.resources.getDrawable(
                    android.R.drawable.star_big_on,
                    null
                )
            )
            false -> button.setImageDrawable(
                App.application.resources.getDrawable(
                    android.R.drawable.star_big_off,
                    null
                )
            )
        }
    }

    override fun onClick(v: View?) {
        clickListener.invoke(adapterPosition, animalType.text.toString())
    }
}