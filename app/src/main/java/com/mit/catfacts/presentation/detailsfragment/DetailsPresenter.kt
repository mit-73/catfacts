package com.mit.catfacts.presentation.detailsfragment

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.mit.catfacts.data.repository.ApiRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

@InjectViewState
class DetailsPresenter : MvpPresenter<DetailsView>() {

    override fun attachView(view: DetailsView?) {
        super.attachView(view)
    }

    override fun detachView(view: DetailsView?) {
        super.detachView(view)
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        /*
        * Этот метод вызывается тогда, когда к конкретному экземпляру Presenter первый раз будет привязана любая View.
        * А когда к этому Presenter будет привязана другая View, к ней уже будет применено состояние из ViewState.
        * И здесь уже не важно, эта новая View – совсем другая View, или пересозданная в результате смены конфигурации.
        * Этот метод подходит для того, чтобы, например, загрузить список новостей при первом открытии экрана списка новостей.
        * */
    }

    private lateinit var repository: ApiRepository

    fun loadDetail(id: String) {
        GlobalScope.launch {
            val request = repository.getById(id)
            if (request != null)
                viewState.onShowDetails(id, request)
        }
    }
}