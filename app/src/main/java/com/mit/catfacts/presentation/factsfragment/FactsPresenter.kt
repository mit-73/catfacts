package com.mit.catfacts.presentation.factsfragment

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.mit.catfacts.data.repository.ApiRepository
import com.mit.catfacts.utils.ANIMAL_CATS
import com.mit.catfacts.utils.Settings
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

@InjectViewState
class FactsPresenter : MvpPresenter<FactsView>() {

    override fun attachView(view: FactsView?) {
        super.attachView(view)
    }

    override fun detachView(view: FactsView?) {
        super.detachView(view)
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        /*
        * Этот метод вызывается тогда, когда к конкретному экземпляру Presenter первый раз будет привязана любая View.
        * А когда к этому Presenter будет привязана другая View, к ней уже будет применено состояние из ViewState.
        * И здесь уже не важно, эта новая View – совсем другая View, или пересозданная в результате смены конфигурации.
        * Этот метод подходит для того, чтобы, например, загрузить список новостей при первом открытии экрана списка новостей.
        * */

        loadFacts()
    }

    private lateinit var repository: ApiRepository

    private var isLoading = false
    private var animal = ""

    fun setAnimal(animal: String) {
        this.animal = animal
    }

    fun updateFavorite(id: String, isFavorite: Boolean) {
        GlobalScope.launch {
            repository.updateFavorite(id, isFavorite)
        }
        viewState.onFavoriteClick(id, isFavorite)
    }

    fun loadFacts() {
        if (isLoading)
            return
        isLoading = true
        viewState.onStartLoading()
        GlobalScope.launch {
            val facts = if (animal != ANIMAL_CATS)
                repository.getFacts(animal, Settings.factsAmount)
            else
                repository.getFavorite()
            viewState.onFinishLoading()
            if (facts.isNotEmpty()) viewState.addFactsList(facts)
            isLoading = false
        }
        viewState.onFinishLoading()
    }
}