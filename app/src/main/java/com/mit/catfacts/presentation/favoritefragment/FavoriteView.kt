package com.mit.catfacts.presentation.favoritefragment

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.mit.catfacts.domain.dataclass.FactModel

/*
* AddToEndStrategy – добавит пришедшую команду в конец очереди. Используется по умолчанию
* AddToEndSingleStrategy – добавит пришедшую команду в конец очереди команд.
    Причём, если команда такого типа уже есть в очереди, то уже существующая будет удалена
* SingleStateStrategy – очистит всю очередь команд, после чего добавит себя в неё
* SkipStrategy – команда не будет добавлена в очередь, и никак не изменит очередь
 */

interface FavoriteView : MvpView {
    @StateStrategyType(AddToEndSingleStrategy::class)
    fun onFavoriteClick(id: String, isFavorite: Boolean)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun onStartLoading()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun onFinishLoading()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun addFactsList(list: List<FactModel>)

//    @StateStrategyType(SingleStateStrategy::class)
//
//    @StateStrategyType(SkipStrategy ::class)
}