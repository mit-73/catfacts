package com.mit.catfacts.presentation.favoritefragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.mit.catfacts.App
import com.mit.catfacts.R
import com.mit.catfacts.domain.dataclass.FactModel
import com.mit.catfacts.presentation.common.itemselector.ItemSelectorPresentor
import com.mit.catfacts.presentation.common.itemselector.ItemSelectorView
import com.mit.catfacts.utils.Screens
import kotlinx.android.synthetic.main.favorite_fragment.*
import kotlinx.android.synthetic.main.favorite_fragment.view.*

class FavoriteFragment : FavoriteView, ItemSelectorView, MvpAppCompatFragment() {

    private lateinit var adapter: FavoriteAdapter

    @InjectPresenter
    lateinit var favoritePresenter: FavoritePresenter

    @InjectPresenter
    lateinit var itemSelectorPresenter: ItemSelectorPresentor

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?{
        val view = inflater.inflate(R.layout.favorite_fragment, container, false)
        adapter.setOnClickListener(itemSelectorPresenter::onListItemSelected)
        adapter.setFavoriteClicker(favoritePresenter::updateFavorite)
        view.favorite_recView.adapter = adapter
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        favoritePresenter.loadFavorite()
    }

    override fun onFavoriteClick(id: String, isFavorite: Boolean) {
        favoritePresenter.updateFavorite(id, isFavorite)
    }

    override fun addFactsList(list: List<FactModel>) {
        adapter.addFacts(list)
    }

    override fun onStartLoading() {
        favorite_progressBar.visibility = View.VISIBLE
    }

    override fun onFinishLoading() {
        favorite_progressBar.visibility = View.GONE
    }

    override fun onItemSelected(position: Int, id: String) {
        App.application.getRouter().navigateTo(Screens.Facts(id))
    }

}