package com.mit.catfacts.presentation.favoritefragment

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.mit.catfacts.data.repository.ApiRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

@InjectViewState
class FavoritePresenter : MvpPresenter<FavoriteView>() {

    override fun attachView(view: FavoriteView?) {
        super.attachView(view)
    }

    override fun detachView(view: FavoriteView?) {
        super.detachView(view)
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        /*
        * Этот метод вызывается тогда, когда к конкретному экземпляру Presenter первый раз будет привязана любая View.
        * А когда к этому Presenter будет привязана другая View, к ней уже будет применено состояние из ViewState.
        * И здесь уже не важно, эта новая View – совсем другая View, или пересозданная в результате смены конфигурации.
        * Этот метод подходит для того, чтобы, например, загрузить список новостей при первом открытии экрана списка новостей.
        * */

        loadFavorite()
    }

    lateinit var repository: ApiRepository

    fun updateFavorite(id: String, isFavorite: Boolean) {
        GlobalScope.launch {
            repository.updateFavorite(id, isFavorite)
        }
        viewState.onFavoriteClick(id, isFavorite)
    }

    private var isLoading = false

    fun loadFavorite() {
        if (isLoading)
            return
        isLoading = true
        viewState.onStartLoading()
        GlobalScope.launch {
            val favoriteFacts = repository.getFavorite()
            viewState.onFinishLoading()
            if (favoriteFacts.isNotEmpty()) viewState.addFactsList(favoriteFacts)
            isLoading = false
        }
        viewState.onFinishLoading()
    }
}