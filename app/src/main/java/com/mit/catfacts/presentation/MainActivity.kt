package com.mit.catfacts.presentation

import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.mit.catfacts.App
import com.mit.catfacts.R
import com.mit.catfacts.presentation.common.itemselector.ItemSelectorView
import com.mit.catfacts.utils.*
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command
import ru.terrakok.cicerone.commands.Replace


class MainActivity : MvpAppCompatActivity(), MainActivityView, ItemSelectorView {

    @InjectPresenter
    lateinit var mainActivityPresenter: MainActivityPresenter

    private lateinit var navigator: Navigator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.my_toolbar))

        navigator = SupportAppNavigator(this, R.id.mainContainer)

        if (savedInstanceState == null) {
            // Если приложение только запущено, замените всю очередь команд на команду, чтобы отобразить первый экран.
            navigator.applyCommands(arrayOf<Command>(Replace(Screens.Facts(ANIMAL_CATS))))
        }
        App.application.getNavigatorHolder().setNavigator(navigator)
    }

    override fun onResume() {
        super.onResume()
        Toast.makeText(this, "ON RESUME", Toast.LENGTH_LONG).show()
        App.application.getNavigatorHolder().setNavigator(navigator)
    }

    override fun onPause() {
        super.onPause()
        Toast.makeText(this, "ON PAUSE", Toast.LENGTH_LONG).show()
        App.application.getNavigatorHolder().removeNavigator()
    }

    override fun onBackPressed() {
        App.application.getRouter().exit()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.navigation_facts -> App.application.getRouter().navigateTo(Screens.Facts(ANIMAL_CATS))
            R.id.navigation_favorite -> App.application.getRouter().navigateTo(Screens.Favorite())

            R.id.nav_chosen -> mainActivityPresenter.onFragmentSelected(ANIMAL_CHOSEN)
            R.id.nav_cats -> mainActivityPresenter.onFragmentSelected(ANIMAL_CATS)
            R.id.nav_dogs -> mainActivityPresenter.onFragmentSelected(ANIMAL_DOGS)
            R.id.nav_snails -> mainActivityPresenter.onFragmentSelected(ANIMAL_SNAILS)
            R.id.nav_horses -> mainActivityPresenter.onFragmentSelected(ANIMAL_HORSES)
        }

        return true
    }

    override fun onItemSelected(position: Int, id: String) {
        mainActivityPresenter.onDetailsSelected(id)
    }
}