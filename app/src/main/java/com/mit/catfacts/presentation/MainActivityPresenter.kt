package com.mit.catfacts.presentation

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.mit.catfacts.App
import com.mit.catfacts.utils.Screens

@InjectViewState
class MainActivityPresenter : MvpPresenter<MainActivityView>(){

    fun onFragmentSelected(animal : String)= App.application.getRouter().navigateTo(Screens.Facts(animal))

    fun onDetailsSelected(id : String) = App.application.getRouter().navigateTo(Screens.Details(id))
}