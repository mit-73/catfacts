package com.mit.catfacts.presentation.common.itemselector

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

/*
* AddToEndStrategy – добавит пришедшую команду в конец очереди. Используется по умолчанию
* AddToEndSingleStrategy – добавит пришедшую команду в конец очереди команд.
    Причём, если команда такого типа уже есть в очереди, то уже существующая будет удалена
* SingleStateStrategy – очистит всю очередь команд, после чего добавит себя в неё
* SkipStrategy – команда не будет добавлена в очередь, и никак не изменит очередь
 */

interface ItemSelectorView : MvpView {
    @StateStrategyType(AddToEndSingleStrategy::class)
    fun onItemSelected(position: Int, id: String)

//    @StateStrategyType(SingleStateStrategy::class)
//
//    @StateStrategyType(SkipStrategy ::class)
}