package com.mit.catfacts.presentation.factsfragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.mit.catfacts.App
import com.mit.catfacts.R
import com.mit.catfacts.domain.dataclass.FactModel
import com.mit.catfacts.presentation.common.itemselector.ItemSelectorPresentor
import com.mit.catfacts.presentation.common.itemselector.ItemSelectorView
import com.mit.catfacts.presentation.favoritefragment.FavoritePresenter
import com.mit.catfacts.presentation.favoritefragment.FavoriteView
import com.mit.catfacts.utils.Screens
import kotlinx.android.synthetic.main.fact_fragment.*
import kotlinx.android.synthetic.main.fact_fragment.view.*

class FactsFragment : FavoriteView, FactsView, ItemSelectorView, MvpAppCompatFragment() {

    private lateinit var adapter: FactsAdapter

    @InjectPresenter
    lateinit var factsPresenter: FactsPresenter

    @InjectPresenter
    lateinit var itemSelectorPresenter: ItemSelectorPresentor

    @InjectPresenter
    lateinit var favoritePresenter: FavoritePresenter

    private var animal = arguments!!.getString("animal")
    private var previousAnimal = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fact_fragment, container, false)
        adapter.setOnClickListener(itemSelectorPresenter::onListItemSelected)
        adapter.setFavoriteClicker(favoritePresenter::updateFavorite)
        view.facts_recView.adapter = adapter
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (previousAnimal != animal) {
            factsPresenter.setAnimal(animal)
            factsPresenter.loadFacts()
            previousAnimal = animal
        } else factsPresenter.loadFacts()
    }

    override fun onFavoriteClick(id: String, isFavorite: Boolean) {
        factsPresenter.updateFavorite(id, isFavorite)
    }

    override fun onStartLoading() {
        facts_progressBar.visibility = View.VISIBLE
    }

    override fun onFinishLoading() {
        facts_progressBar.visibility = View.GONE
    }

    override fun onItemSelected(position: Int, id: String) {
        App.application.getRouter().navigateTo(Screens.Details(id))
    }

    override fun addFactsList(list: List<FactModel>) {
        adapter.addFacts(list)
    }

    fun animalInstance(animal: String): FactsFragment {
        val factsFragment = FactsFragment()

        val args = Bundle()
        args.putString("animal", animal)
        factsFragment.arguments = args

        return factsFragment
    }
}

