package com.mit.catfacts.presentation.factsfragment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mit.catfacts.R
import com.mit.catfacts.domain.dataclass.FactModel

class FactsAdapter : RecyclerView.Adapter<FactsHolder>() {
    private var clickListener: (Int, String) -> Unit = { _: Int, _: String -> }
    private var favoriteClicker: (String, Boolean) -> Unit = { _: String, _: Boolean -> }
    private var factModels: List<FactModel> = listOf()

    fun setOnClickListener(listener: (Int, String) -> Unit) {
        clickListener = listener
    }

    fun setFavoriteClicker(clicker: (String, Boolean) -> Unit) {
        favoriteClicker = clicker
    }

    fun addFacts(factModels: List<FactModel>) {
        this.factModels = factModels
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return factModels.size
    }

    override fun onBindViewHolder(holder: FactsHolder, position: Int) {
        val factItem = factModels[position]
        holder.factText.text = factItem.text
        holder.animalType.text = factItem.id
        holder.setFavoriteState(factItem.isFavorite)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FactsHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_fact, parent, false)
        return FactsHolder(view, clickListener, favoriteClicker)
    }
}