package com.mit.catfacts.presentation.common.itemselector

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter

@InjectViewState
class ItemSelectorPresentor : MvpPresenter<ItemSelectorView>() {

    override fun attachView(view: ItemSelectorView?) {
        super.attachView(view)
    }

    override fun detachView(view: ItemSelectorView?) {
        super.detachView(view)
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        /*
        * Этот метод вызывается тогда, когда к конкретному экземпляру Presenter первый раз будет привязана любая View.
        * А когда к этому Presenter будет привязана другая View, к ней уже будет применено состояние из ViewState.
        * И здесь уже не важно, эта новая View – совсем другая View, или пересозданная в результате смены конфигурации.
        * Этот метод подходит для того, чтобы, например, загрузить список новостей при первом открытии экрана списка новостей.
        * */
    }

    fun onListItemSelected(position: Int, id: String) {
        viewState.onItemSelected(position, id)
    }
}