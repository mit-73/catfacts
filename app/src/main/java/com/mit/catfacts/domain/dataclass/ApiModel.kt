package com.mit.catfacts.domain.dataclass

data class ApiModel(
    val _id: String,
    val __v: Int,
    val text: String,
    val delete: Boolean,
    val source: String,
    val used: Boolean,
    val type: String,
    val updatedAt: String,
    val createdAt: String
)