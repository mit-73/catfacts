package com.mit.catfacts.domain.entities

import android.app.Application
import androidx.room.Room
import com.mit.catfacts.data.db.CatFactsDatabase
import com.mit.catfacts.data.db.dao.FactsDAO
import com.mit.catfacts.data.db.dao.FavoriteDAO
import com.mit.catfacts.utils.DB_NAME

class DatabaseEntity {
    fun provideDatabase(application: Application): CatFactsDatabase {
        return Room.databaseBuilder(
            application.applicationContext,
            CatFactsDatabase::class.java, DB_NAME
        ).build()
    }

    fun provideFactsDAO(currencyDatabase: CatFactsDatabase): FactsDAO = currencyDatabase.factsDAO()

    fun provideFavoriteDAO(currencyDatabase: CatFactsDatabase): FavoriteDAO = currencyDatabase.favoriteDao()
}