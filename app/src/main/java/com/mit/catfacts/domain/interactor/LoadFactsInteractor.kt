package com.mit.catfacts.domain.interactor

import android.util.Log
import com.mit.catfacts.App
import com.mit.catfacts.data.db.entity.FactsEntity
import kotlinx.coroutines.*

object LoadFactsInteractor {

    fun executeSavedFacts(animal: String, onComplete: ((List<FactsEntity>) -> Unit)?){
        CoroutineScope(Dispatchers.IO).launch {
            val db = App.databaseEntity
            val dbDAO = db.provideFactsDAO(db.provideDatabase(App.application))
            try {
                withContext(coroutineContext) {
                    onComplete?.invoke(dbDAO.getSavedFacts(animal))
                }
            } catch (e: CancellationException) {
                Log.v("Logs", "canceled by user")
            } catch (e: Exception) {
                Log.v("Logs", "$e")
            }
        }
    }

    fun executeText(id: String, onComplete: ((String) -> Unit)?){
        CoroutineScope(Dispatchers.IO).launch {
            val db = App.databaseEntity
            val dbDAO = db.provideFactsDAO(db.provideDatabase(App.application))
            try {
                withContext(coroutineContext) {
                    onComplete?.invoke(dbDAO.getText(id))
                }
            } catch (e: CancellationException) {
                Log.v("Logs", "canceled by user")
            } catch (e: Exception) {
                Log.v("Logs", "$e")
            }
        }
    }

    fun executeSave(id: String, onComplete: ((String) -> Unit)?){
        CoroutineScope(Dispatchers.IO).launch {
            val db = App.databaseEntity
            val dbDAO = db.provideFactsDAO(db.provideDatabase(App.application))
            try {
                withContext(coroutineContext) {
                    onComplete?.invoke(dbDAO.getRecord(id).text)
                }
            } catch (e: CancellationException) {
                Log.v("Logs", "canceled by user")
            } catch (e: Exception) {
                Log.v("Logs", "$e")
            }
        }
    }

    fun executeInsertFacts(facts: List<FactsEntity>, onComplete: ((Unit) -> Unit)?){
        CoroutineScope(Dispatchers.IO).launch {
            val db = App.databaseEntity
            val dbDAO = db.provideFactsDAO(db.provideDatabase(App.application))
            try {
                withContext(coroutineContext) {
                    onComplete?.invoke(dbDAO.insertFacts(facts))
                }
            } catch (e: CancellationException) {
                Log.v("Logs", "canceled by user")
            } catch (e: Exception) {
                Log.v("Logs", "$e")
            }
        }
    }
}