package com.mit.catfacts.domain.interactor

import android.util.Log
import com.mit.catfacts.App
import com.mit.catfacts.domain.dataclass.ApiModel
import kotlinx.coroutines.*

object ApiInteractor {

    fun executeRandom(animalType: String, amount: Int, onComplete: ((List<ApiModel>) -> Unit)?) {

        CoroutineScope(Dispatchers.IO).launch {
            var api = App.apiEntity
            val service = api.provideApi(api.provideRetrofit(api.provideGson(), api.provideOkHttpClient()))
            val response = service.getRandomFactsAsync(animalType, amount).await()
            try {
                withContext(coroutineContext) {
                    onComplete?.invoke(
                        response.body().orEmpty()
                    )
                }
            } catch (e: CancellationException) {
                Log.v("Logs", "canceled by user")
            } catch (e: Exception) {
                Log.v("Logs", "$e")
            }
        }
    }

    fun executeId(id: String, onComplete: ((String) -> Unit)?) {

        CoroutineScope(Dispatchers.IO).launch {
            var api = App.apiEntity
            val service = api.provideApi(api.provideRetrofit(api.provideGson(), api.provideOkHttpClient()))
            val response = service.getFactByIdAsync(id).await()
            try {
                withContext(coroutineContext) {
                    onComplete?.invoke(response.body().toString())
                }
            } catch (e: CancellationException) {
                Log.v("Logs", "canceled by user")
            } catch (e: Exception) {
                Log.v("Logs", "$e")
            }
        }
    }

}