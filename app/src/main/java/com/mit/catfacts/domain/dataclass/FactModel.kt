package com.mit.catfacts.domain.dataclass

data class FactModel(
    var id: String?,
    var text: String?,
    var isFavorite: Boolean
)