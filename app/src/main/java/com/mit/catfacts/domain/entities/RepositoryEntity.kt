package com.mit.catfacts.domain.entities

import com.mit.catfacts.data.repository.ApiRepository
import com.mit.catfacts.domain.interactor.ApiInteractor
import com.mit.catfacts.domain.interactor.LoadFactsInteractor
import com.mit.catfacts.domain.interactor.LoadFavoriteInteractor

class RepositoryEntity {
    fun provideApiRepository(api : ApiInteractor, dbFacts: LoadFactsInteractor, dbFavorite: LoadFavoriteInteractor): ApiRepository =
        ApiRepository(api, dbFacts, dbFavorite)
}