package com.mit.catfacts.domain.interactor

import android.util.Log
import com.mit.catfacts.App
import com.mit.catfacts.data.db.entity.FavoriteEntity
import kotlinx.coroutines.*

object LoadFavoriteInteractor {

    fun executeAddFavorite(unit: FavoriteEntity, onComplete: ((Unit) -> Unit)?){
        CoroutineScope(Dispatchers.IO).launch {
            val db = App.databaseEntity
            val dbDAO = db.provideFavoriteDAO(db.provideDatabase(App.application))
            try {
                withContext(coroutineContext) {
                    onComplete?.invoke(dbDAO.addFavorite(unit))
                }
            } catch (e: CancellationException) {
                Log.v("Logs", "canceled by user")
            } catch (e: Exception) {
                Log.v("Logs", "$e")
            }
        }
    }

    fun executeGetFavorites(): List<FavoriteEntity> {
        var dbDAO: List<FavoriteEntity>? = null
        CoroutineScope(Dispatchers.IO).launch {
            val db = App.databaseEntity
            dbDAO = db.provideFavoriteDAO(db.provideDatabase(App.application)).getFavorites()

        }
        return dbDAO!!
    }

    fun executeRemoveFavorite(id: String, onComplete: ((Unit) -> Unit)?){
        CoroutineScope(Dispatchers.IO).launch {
            val db = App.databaseEntity
            val dbDAO = db.provideFavoriteDAO(db.provideDatabase(App.application))
            try {
                withContext(coroutineContext) {
                    onComplete?.invoke(dbDAO.removeFavorite(id))
                }
            } catch (e: CancellationException) {
                Log.v("Logs", "canceled by user")
            } catch (e: Exception) {
                Log.v("Logs", "$e")
            }
        }
    }
}