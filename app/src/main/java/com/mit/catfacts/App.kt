package com.mit.catfacts

import android.app.Application
import com.mit.catfacts.domain.entities.ApiEntity
import com.mit.catfacts.domain.entities.DatabaseEntity
import com.mit.catfacts.domain.entities.RepositoryEntity
import com.mit.catfacts.domain.interactor.ApiInteractor
import com.mit.catfacts.domain.interactor.LoadFactsInteractor
import com.mit.catfacts.domain.interactor.LoadFavoriteInteractor
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router

class App : Application() {

    private lateinit var cicerone: Cicerone<Router>

    companion object {
        lateinit var application: App

        var apiEntity = ApiEntity()
        var databaseEntity = DatabaseEntity()
        var repositoryEntity = RepositoryEntity()
    }

    override fun onCreate() {
        super.onCreate()
        application = this

        repositoryEntity.provideApiRepository(ApiInteractor, LoadFactsInteractor, LoadFavoriteInteractor)

        initCicerone()
    }

    private fun initCicerone() {
        cicerone = Cicerone.create()
    }

    fun getNavigatorHolder(): NavigatorHolder {
        return cicerone.navigatorHolder
    }

    fun getRouter(): Router {
        return cicerone.router
    }

}